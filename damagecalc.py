from weapon import Weapon
from enemy import Enemy, damage_multiplier

def damage(wpn: Weapon, dmg_type: str = None) -> dict[str: float] | float:
        if dmg_type:
            return wpn.damage[dmg_type]
        else:
            return {dt: round(d, 1) for dt, d in wpn.damage.items()}

def avg_damage(wpn: Weapon, dmg_type: str = None) -> dict[str: float] | float:
        if dmg_type:
            return wpn.damage[dmg_type]*wpn.avg_crit*wpn.multishot
        else:
            return {dt: round(d*wpn.avg_crit*wpn.multishot,1)
                    for dt, d in wpn.damage.items()}

def effective_damage(wpn: Weapon, healthtype, armortype: str = None,
                         armor: int = 0) -> dict[str: float]:
        dmg_mult = damage_multiplier(healthtype, armortype, armor)
        return {dt: round(d*dmg_mult[dt],1)
                for dt, d in wpn.damage.items()}

def effective_damage_enemy(wpn: Weapon, enemy: Enemy, shield: bool = False) -> dict[str: float]:
        if shield:
            ht = enemy.shield_type
            at = None
            a = 0
        else:
            ht = enemy.health_type
            at = enemy.armor_type
            a = enemy.armor
        return effective_damage(wpn = wpn, healthtype = ht, armortype = at, armor = a)

def avg_effective_damage(wpn: Weapon, healthtype, armortype: str = None,
                             armor: int = 0) -> dict[str: float]:
        dmg_mult = damage_multiplier(healthtype, armortype, armor)
        return {dt: round(d*dmg_mult[dt]*wpn.avg_crit*wpn.multishot,1)
                for dt, d in wpn.damage.items()}

def avg_effective_damage_enemy(wpn: Weapon, enemy: Enemy, shield: bool = False) -> dict[str: float]:
        if shield:
            ht = enemy.shield_type
            at = None
            a = 0
        else:
            ht = enemy.health_type
            at = enemy.armor_type
            a = enemy.armor
        return avg_effective_damage(wpn = wpn, healthtype = ht, armortype = at, armor = a)

def dps(wpn: Weapon, sustained: bool=False) -> float:
        d = (sum(wpn.damage.values())
                * wpn._avg_crit
                * wpn.multishot
                * wpn.atk_rate)
        s = wpn._tte/wpn._spm if sustained else 1
        return round(d * s, 1)

def effective_dps(wpn: Weapon, healthtype, armortype: str = None,
                      armor: int = 0, sustained: bool=False) -> dict[str: float]:
        dmg_mult = damage_multiplier(healthtype, armortype, armor)
        return (round(sum([d*dmg_mult[dt]
                     for dt, d in wpn.damage.items()])
                * wpn._avg_crit
                * wpn.multishot
                * wpn.atk_rate,1))

def effective_dps_enemy(wpn: Weapon, enemy: Enemy, shield: bool = False, sustained: bool=False) -> dict[str: float]:
        if shield:
            ht = enemy.shield_type
            at = None
            a = 0
        else:
            ht = enemy.health_type
            at = enemy.armor_type
            a = enemy.armor
        return effective_dps(wpn = wpn, healthtype = ht, armortype = at, armor = a, sustained = sustained)
