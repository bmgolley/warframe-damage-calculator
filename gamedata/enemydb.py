import pandas as pd

e = pd.read_json('gamedata/enemies/Enemy.json')
e.rename(columns={'name': 'enemy', 'health': 'base_health', 'shield': 'base_shield', 'armor': 'base_armor'}, inplace=True)
e.set_index('enemy', inplace=True)
e['st'], e['at'], e['ht'] = zip(*list(e.pop('resistances').values))
e['shield_type'] = [s['type'] for s in e['st']]
e['armor_type'] = [a['type'] for a in e['at']]
e['health_type'] = [h['type'] for h in e['ht']]
e = e.filter(items=['base_health', 'base_shield', 'base_armor','shield_type','armor_type','health_type'])
e.drop_duplicates(inplace=True)

# try:
#     c = pd.read_excel('gamedata/enemies/user_defined.xlsx')
#     e = e.merge(c, how='left')
# except:
#     pass

def enemydata(name: str) -> dict:
    return e.loc[name].to_dict()