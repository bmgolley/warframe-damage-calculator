import pandas as pd

w = pd.read_json('gamedata/weapons/Primary.json')

w = w.filter(items=['name', 'attacks', 'magazineSize', 'multishot', 'reloadTime',
       'trigger', 'type'])
w = w.explode('attacks', ignore_index=True)
w.rename(columns={'name': 'weapon'}, inplace=True)
a = pd.DataFrame(w.pop('attacks').values.tolist())
w = w.join(a)
w.rename(columns={'magazineSize': 'magazine_size', 'reloadTime': 'reload_time', 
                  'name': 'attack', 'speed': 'atk_spd'}, inplace=True)
w.drop(columns=['shot_type','shot_speed', 'flight', 'falloff'], inplace=True)
w['crit_chance'] /= 100
w['status_chance'] /= 100
w.fillna(value={'magazine_size': 1, 'charge_time': 0}, inplace=True)
try:
   c = pd.read_excel('gamedata/weapons/user_defined.xlsx')
   w = w.merge(c, how='left')
except:
   pass

def weapondata(name: str, attack: str=None) -> dict:
    d = w[w['weapon'] == name]
    if attack:
       return {k: v[0] for k,v in d[d['attack']==attack].to_dict('list').items()}
    else:
       return {k: v[0] for k,v in d.to_dict('list').items()}