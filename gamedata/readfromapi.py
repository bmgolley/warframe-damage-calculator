import urllib3
import shutil

def updategamedata(filename: str, folder: str) -> None:
    conn = urllib3.PoolManager()
    with open('GameData/' + folder + '/' + filename + '.json', 'wb') as f:
        data = conn.request('GET', 'https://raw.githubusercontent.com/WFCD/warframe-items/master/data/json/' + filename + '.json', preload_content = False)
        shutil.copyfileobj(data, f)

updategamedata("Enemy", "enemies")
updategamedata("Primary", "weapons")
updategamedata("Secondary", "weapons")
updategamedata("Melee", "weapons")
updategamedata("Arch-Gun", "weapons")
updategamedata("Arch-Melee", "weapons")
updategamedata("SentinelWeapons", "weapons")