from math import sqrt
import pandas as pd
import weapon
from gamedata import enemydata
from statusEffect import AppliedStatusEffects, StatusEffect


class Enemy:

    @staticmethod
    def game_enemy(name: str, level: int):
        return Enemy(level=level, base_level=1, **enemydata(name))

    def __init__(self, level: int, base_level: int, base_health: int,
                 health_type: str, base_shield: int = 0, 
                 shield_type: str = None, base_armor: int = 0, 
                 armor_type: str = None) -> None:

        self.level = level
        self.base_health = base_health
        self.base_shield = base_shield
        self.base_armor = base_armor
        self.base_level = base_level
        self.shield_type = shield_type
        self.health_type = health_type
        self.armor_type = armor_type

        if base_level > 0:
            self.starting_stats()
        else:
            self.starting_health = self.base_health
            self.starting_shield = self.base_shield
            self.starting_armor = self.base_armor

        self.health = self.starting_health
        self.shield = self.starting_shield
        self.armor = self.starting_armor

        self.shield_gating = False

        self._health_modifiers: dict[str: float] = healthtypes[health_type]
        self._shield_modifiers: dict[str: float] = (shieldtypes[shield_type]
                                                    if shield_type else None)
        self._armor_modifiers: dict[str: float] = (armortypes[armor_type]
                                                   if armor_type else None)

        self._status_effects = {dmg_type:
                                AppliedStatusEffects(proctype=dmg_type)
                                for dmg_type in weapon.damagetypes}
        self._statuseffectcount: dict[str: int] = {}
        self._heat_proc_t = 0
        self._heat_proc_mult = 0
        self.corrosive_projection_count = 0
        self._corrosive_projection_mult = 0
        self._corrosive_proc_count = 0
        self._corrosive_proc_mult = 0
        self._shieldmult = 1
        self._healthmult = 1
        self._status = pd.DataFrame({
            "Time": 0,
            "Shields": self.shield,
            "Health": self.health,
            "Armor": self.armor
        }, index=[0])

        self._dead = False

    def _update_status(self, t: float, wpn: weapon.Weapon) -> None:
        StatusEffect._wpn = wpn
        AppliedStatusEffects._wpn = wpn
        for stat in self._status_effects.values():
            stat.update(t, wpn)
            if (c := stat.count()) > 0:
                self._statuseffectcount[stat.proctype] = c
            else:
                self._statuseffectcount.pop(stat.proctype, None)
        self._corrosive_projection_mult = (1
            - (0.18*self.corrosive_projection_count))
        self._corrosive_proc_mult = self._status_effects['corrosive'].armormult
        self._heat_proc_mult = self._status_effects['heat'].armormult
        self._shieldmult = self._status_effects['magnetic'].damagemult
        self._healthmult = self._status_effects['viral'].damagemult
        self.armor = (self.starting_armor * self._corrosive_projection_mult
                      * self._corrosive_proc_mult * self._heat_proc_mult)

    def starting_stats(self, level: int = None) -> None:
        if not level:
            level = self.level
        else:
            self.level = level
        dLevel = level - self.base_level
        T = (dLevel - 70)/10
        if dLevel < 70:
            S = 0
        elif 70 < dLevel <= 80:
            S = 3*T**2 - 2*T**3
        else:
            S = 1

        hm1 = 1 + 0.015 * dLevel**2
        hm2 = 1 + 24*sqrt(5)/5 * dLevel**0.5
        hm = (hm1 * (1-S)) + (hm2*S)

        sm1 = 1 + 0.02 * dLevel**1.75
        sm2 = 1 + 1.6 * dLevel**0.75
        sm = (sm1 * (1-S)) + (sm2*S)

        am1 = 1 + 0.005 * dLevel**1.75
        am2 = 1 + 0.4 * dLevel**0.75
        am = (am1 * (1-S)) + (am2*S)

        self.starting_health = self.base_health * hm
        self.starting_shield = self.base_shield * sm
        self.starting_armor = self.base_armor * am

    def inflict_damage(self, wpn: weapon.Weapon, t: float = None) -> None:
        import damagecalc as dc
        def _inflict_dmg_type(_dmg_type: str, _dmg: float) -> tuple[float, float, float]:
            health_mod = self._health_modifiers[_dmg_type]
            shield_mod = (self._shield_modifiers[_dmg_type]
                          if self._shield_modifiers else None)
            armor_mod = (self._armor_modifiers[_dmg_type]
                         if self._armor_modifiers else None)
            _s = _h = _p = 0
            if self._shield_modifiers and self.shield > 0 and _dmg_type != "toxin":
                _s = _dmg*(1 + shield_mod)*self._shieldmult
            if self._armor_modifiers and armor_mod < 1 and self.armor > 0:
                health_mult = (300/(300 + self.armor*(1 - armor_mod))
                    * (1 + armor_mod) * (1 + health_mod) * self._healthmult)
            else:
                health_mult = (1 + health_mod) * self._healthmult
            if _dmg_type == "toxin":
                _h = _dmg * health_mult
            else:
                _p = _dmg * health_mult
            return _s, _h, _p

        self._update_status(t, wpn)
        shield_damage = 0
        health_damage = 0
        health_damage_potential = 0
        for dmg_type, dmg in dc.avg_damage(wpn).items():
            if dmg > 0:
                if t:
                    stat = self._status_effects[dmg_type]
                    if dmg_type == stat.damagetype:
                        dmg += stat.totaldamage
                    elif stat.damagetype and stat.totaldamage:
                        s, h, p = _inflict_dmg_type(
                            stat.damagetype, stat.totaldamage)
                        shield_damage += s
                        health_damage += h
                        health_damage_potential += p
                s, h, p = _inflict_dmg_type(dmg_type, dmg)
                shield_damage += s
                health_damage += h
                health_damage_potential += p

        if self.shield > 0:
            self.shield -= shield_damage
            if self.shield < 0:
                health_damage_potential *= abs(self.shield)/shield_damage if not self.shield_gating else 0
                self.shield = 0
            else:
                health_damage_potential = 0

        health_damage += health_damage_potential
        self.health -= health_damage
        self.health = max(self.health, 0)
        self._dead = not self.health

    def kill(self, weapon: weapon.Weapon):
        if self._dead:
            self.reset()
        t = 0
        while not self._dead:
            self.inflict_damage(weapon, t)
            self._status = pd.concat([self._status, pd.DataFrame({
                "Time": t,
                "Shields": self.shield,
                "Health": self.health,
                "Armor": self.armor
            } | self._statuseffectcount, index=[0])], ignore_index=True)
            t += 1/weapon.atk_rate
            self._status = self._status.round(1)
        return self._status

    def reset(self) -> None:
        self.health = self.starting_health
        self.shield = self.starting_shield
        self.armor = self.starting_armor
        self._status_effects = {dmg_type:
                                AppliedStatusEffects(proctype=dmg_type)
                                for dmg_type in weapon.damagetypes}
        self.corrosive_projection_count = 0
        self._corrosive_projection_mult = 0
        self._corrosive_proc_count = 0
        self._corrosive_proc_mult = 0
        self._status = pd.DataFrame({
            "Time": 0,
            "Shields": self.shield,
            "Health": self.health,
            "Armor": self.armor
        }, index=[0])
        self._dead = False


def damage_multiplier(healthtype: str, armortype: str = None,
                      armor: int = 0) -> dict:
    
    if healthtype in shieldtypes.keys():
        dmg_mult = dict.fromkeys(shieldtypes[healthtype])
        for dmg_type, shield_mod in shieldtypes[healthtype].items():
            dmg_mult[dmg_type] = (1 + shield_mod) if dmg_type != "toxin" else 0
    elif healthtype in healthtypes.keys():
        dmg_mult = dict.fromkeys(healthtypes[healthtype])
        for dmg_type, health_mod in healthtypes[healthtype].items():
            if (armortype in armortypes.keys() and
                    (armor_mod := armortypes[armortype][dmg_type]) < 1 and armor > 0):
                dmg_mult[dmg_type] = (300 /
                                      (300+armor*(1-armor_mod))
                                      * (1+armor_mod) * (1+health_mod))
            else:
                dmg_mult[dmg_type] = (1 + health_mod)
    return dmg_mult


shieldtypes = {
    "Tenno Shield": {
        "impact": -0.25,
        "puncture": -0.25,
        "slash": -0.25,
        "cold": -0.25,
        "electricity": -0.25,
        "heat": -0.25,
        "toxin": 1,
        "blast": -0.25,
        "corrosive": -0.25,
        "gas": -0.25,
        "magnetic": -0.25,
        "radiation": -0.25,
        "viral": -0.25,
        "truedmg": 0,
        "void": -0.25,
        "tau": -0.25
    },
    "Proto Shield": {
        "impact": 0.15,
        "puncture": -0.5,
        "slash": 0,
        "cold": 0,
        "electricity": 0,
        "heat": -0.5,
        "toxin": 1,
        "blast": 0,
        "corrosive": -0.5,
        "gas": 0,
        "magnetic": 0.75,
        "radiation": 0,
        "viral": 0,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    },
    "Shield": {
        "impact": 0.5,
        "puncture": -0.2,
        "slash": 0,
        "cold": 0.5,
        "electricity": 0,
        "heat": 0,
        "toxin": 1,
        "blast": 0,
        "corrosive": 0,
        "gas": 0,
        "magnetic": 0.75,
        "radiation": -0.25,
        "viral": 0,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    },
    "Untyped": {
        "impact": 0,
        "puncture": 0,
        "slash": 0,
        "cold": 0,
        "electricity": 0,
        "heat": 0,
        "toxin": 0,
        "blast": 0,
        "corrosive": 0,
        "gas": 0,
        "magnetic": 0,
        "radiation": 0,
        "viral": 0,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    }
}

healthtypes = {
    "Tenno Flesh": {
        "impact": 0,
        "puncture": 0,
        "slash": 0,
        "cold": 0,
        "electricity": 0,
        "heat": 0,
        "toxin": 0,
        "blast": 0,
        "corrosive": 0,
        "gas": 0,
        "magnetic": 0,
        "radiation": 0,
        "viral": 0,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    },
    "Machinery": {
        "impact": 0.25,
        "puncture": 0,
        "slash": 0,
        "cold": 0,
        "electricity": 0.5,
        "heat": 0,
        "toxin": -0.25,
        "blast": 0.75,
        "corrosive": 0,
        "gas": 0,
        "magnetic": 0,
        "radiation": 0,
        "viral": -0.25,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    },
    "Cloned Flesh": {
        "impact": -0.25,
        "puncture": 0,
        "slash": 0.25,
        "cold": 0,
        "electricity": 0,
        "heat": 0.25,
        "toxin": 0,
        "blast": 0,
        "corrosive": 0,
        "gas": -0.5,
        "magnetic": 0,
        "radiation": 0,
        "viral": 0.75,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    },
    "Fossilized": {
        "impact": 0,
        "puncture": 0,
        "slash": 0.15,
        "cold": -0.25,
        "electricity": 0,
        "heat": 0,
        "toxin": -0.5,
        "blast": 0.5,
        "corrosive": 0.75,
        "gas": 0,
        "magnetic": 0,
        "radiation": -0.75,
        "viral": 0,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    },
    "Infested Flesh": {
        "impact": 0,
        "puncture": 0,
        "slash": 0.5,
        "cold": -0.5,
        "electricity": 0,
        "heat": 0.5,
        "toxin": 0,
        "blast": 0,
        "corrosive": 0,
        "gas": 0.5,
        "magnetic": 0,
        "radiation": 0,
        "viral": 0,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    },
    "Infested": {
        "impact": 0,
        "puncture": 0,
        "slash": 0.25,
        "cold": 0,
        "electricity": 0,
        "heat": 0.25,
        "toxin": 0,
        "blast": 0,
        "corrosive": 0,
        "gas": 0.75,
        "magnetic": 0,
        "radiation": -0.5,
        "viral": -0.5,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    },
    "Robotic": {
        "impact": 0,
        "puncture": 0.25,
        "slash": -0.25,
        "cold": 0,
        "electricity": 0.5,
        "heat": 0,
        "toxin": -0.25,
        "blast": 0,
        "corrosive": 0,
        "gas": 0,
        "magnetic": 0,
        "radiation": 0.25,
        "viral": 0,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    },
    "Flesh": {
        "impact": -0.25,
        "puncture": 0,
        "slash": 0.25,
        "cold": 0,
        "electricity": 0,
        "heat": 0,
        "toxin": 0.5,
        "blast": 0,
        "corrosive": 0,
        "gas": -0.25,
        "magnetic": 0,
        "radiation": 0,
        "viral": 0.5,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    },
    "Untyped": {
        "impact": 0,
        "puncture": 0,
        "slash": 0,
        "cold": 0,
        "electricity": 0,
        "heat": 0,
        "toxin": 0,
        "blast": 0,
        "corrosive": 0,
        "gas": 0,
        "magnetic": 0,
        "radiation": 0,
        "viral": 0,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    }
}

armortypes = {
    "Infested Sinew": {
        "impact": 0,
        "puncture": 0.25,
        "slash": 0,
        "cold": 0.25,
        "electricity": 0,
        "heat": 0,
        "toxin": 0,
        "blast": -0.5,
        "corrosive": 0,
        "gas": 0,
        "magnetic": 0,
        "radiation": 0.5,
        "viral": 0,
        "truedmg": 1,
        "void": 0,
        "tau": 0
    },
    "Alloy Armor": {
        "impact": 0,
        "puncture": 0.15,
        "slash": -0.5,
        "cold": 0.25,
        "electricity": -0.5,
        "heat": 0,
        "toxin": 0,
        "blast": 0,
        "corrosive": 0,
        "gas": 0,
        "magnetic": -0.5,
        "radiation": 0.75,
        "viral": 0,
        "truedmg": 1,
        "void": 0,
        "tau": 0
    },
    "Ferrite Armor": {
        "impact": 0,
        "puncture": 0.5,
        "slash": -0.15,
        "cold": 0,
        "electricity": 0,
        "heat": 0,
        "toxin": 0,
        "blast": -0.25,
        "corrosive": 0.75,
        "gas": 0,
        "magnetic": 0,
        "radiation": 0,
        "viral": 0,
        "truedmg": 1,
        "void": 0,
        "tau": 0
    },
    "Tenno Armor": {
        "impact": 0,
        "puncture": 0,
        "slash": 0,
        "cold": 0,
        "electricity": 0,
        "heat": 0,
        "toxin": 0,
        "blast": 0,
        "corrosive": 0,
        "gas": 0,
        "magnetic": 0,
        "radiation": 0,
        "viral": 0,
        "truedmg": 1,
        "void": 0,
        "tau": 0
    },
    "Untyped": {
        "impact": 0,
        "puncture": 0,
        "slash": 0,
        "cold": 0,
        "electricity": 0,
        "heat": 0,
        "toxin": 0,
        "blast": 0,
        "corrosive": 0,
        "gas": 0,
        "magnetic": 0,
        "radiation": 0,
        "viral": 0,
        "truedmg": 0,
        "void": 0,
        "tau": 0
    }
}
