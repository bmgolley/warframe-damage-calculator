from gamedata import weapondata

class Weapon:

    @staticmethod
    def game_weapon(name: str, attack: str=None):
        return Weapon(**weapondata(name, attack))

    def __init__(self, damage: dict[str, float], atk_spd: float,
                 crit_chance: float, crit_mult: float, status_chance: float, reload_time: float,magazine_size: int=1,
                 charge_time: float = 0, multishot: float = 1, guaranteed_proc: str=None, **_) -> None:
        # Store initial/base stats
        self.base_damage = {dt.lower(): d for dt, d in damage.items()}
        """ Original unmodified damage types and values """
        self.damage = self.base_damage.copy()
        self._noprogdmg = self.base_damage.copy()
        self._moddamage = self.damage.copy()
        """ Current weapon damage types and values """
        self._totaldamage = sum(self.damage.values())
        """ Sum of all damage types. """
        self._totalbasedamage = self._totaldamage
        """ Sum of original damage values """
        self._totalnoprogdmg = self._totaldamage
        self.atk_spd = self.base_atk_spd = atk_spd
        self.crit_chance = self.base_crit_chance = crit_chance
        self.crit_mult = self.base_crit_mult = crit_mult
        self.status_chance = self.base_status_chance = status_chance
        self.charge_time = self.base_charge_time = charge_time
        self.multishot = self.base_multishot = multishot
        self.magazine_size = self.base_magazine_size = magazine_size
        self.reload_time = self.base_reload_time = reload_time
        self.guaranteed_proc = guaranteed_proc
        # for bows: self.atk_rate = 1 / (self.charge_time*self.base_atk_spd + self.reload_time)
        self.atk_rate = 1/(self.charge_time + 1/self.atk_spd)
        """ The actual number of attacks per second """
        self._tte = self.magazine_size/self.atk_rate
        """ Time to empty the magazine """
        self._spm = self._tte + self.reload_time
        """ Total time from from fresh magazine to fresh magazine """

        self.slashonimpact = None

        self.dmg_per_status = 0.0
        """
        Sets extra base damage that is affected by number of types of status effects.
        Condition Overload, Galvanized Aptitude, etc.
        """
        
        self._statcount = 0

        self._bonuses = {
            "base": {},
            "faction": {},
            "atkspd": {},
            "critchance": {},
            "critmult": {},
            "statchance": {},
            "chargetime": {},
            "multishot": {},
            "magazinesize": {},
            "reloadtime": {}
        } #| dict.fromkeys(damagetypes)
        """ Holds bonuses for all stats """
        self._bonuses |= {t: dict() for t in damagetypes}

        self._totalbonus = dict.fromkeys(self._bonuses.keys(), 0.0)
        """ The sum of bonuses for each type """

        self._appliedbonuses = {}
        """ Holds the names and types of all applied bonuses """

        self._recalc = False
        """ Flag to indicate the need to recalculate damage """

    def set_progenitor_bonus(self, type: str, percent: float) -> None:
        self.base_damage = self._noprogdmg.copy()
        if percent < 0.25 or percent > 0.6:
            raise Exception("Progenitor bonus out of bounds.")
        self.base_damage[type] = self.base_damage.get(type, 0) + percent*self._totalnoprogdmg
        self._totalbasedamage = sum(self.base_damage.values())
        if type in physical:
            self._moddamage[type] = self.base_damage[type] * (1 + self._totalbonus[type])
        elif type in elements:
            self._moddamage[type] = self.base_damage[type] + self._totalbasedamage * self._totalbonus[type]


    def add_special(self, *mods: dict[str, float]) -> None:
        for mod in mods:
            for name, value in mod.items():
                match name.lower():
                    case "internal bleeding" | "internalbleeding" | "internal_bleeding" | "hemorrhage":
                        self.slashonimpact = value

    def add_bonuses(self, mods: dict[str, dict[str, float]]) -> None:
        """ Apply multiple new bonus. Each bonus can affect multiple stats """
        self._appliedbonuses |= mods
        for name, bonuses in mods.items():
            for bonustype, bonus in bonuses.items():
                self._add_remove_bonus(
                    name=name, bonustype=bonustype, bonus=bonus, add=True)
        if self._recalc:
            self._recalculate_base()


    def add_bonus(self, name: str, bonuses: dict[str: float]) -> None:
        """ Apply a new bonus. Each bonus can affect multiple stats """
        self._appliedbonuses |= {name: bonuses}
        for bonustype, bonus in bonuses.items():
            self._add_remove_bonus(
                name=name, bonustype=bonustype, bonus=bonus, add=True)
        if self._recalc:
            self._recalculate_base()

    def remove_bonuses(self, *names):
        """ Remove named bonuses."""
        for name in names:
            for bonustype in self._appliedbonuses[name]:
                self._add_remove_bonus(
                    name=name, bonustype=bonustype, bonus=0, add=False)
            self._appliedbonuses.pop(name, None)
        if self._recalc:
            self._recalculate_base()

    def remove_bonus(self, name: str, bonus: dict[str: float] = None) -> None:
        """
        Remove an already applied bonus.
        Can remove individul stat bonuses, or all stat bonuses from a specific bonus
        """
        if bonus:
            for bonustype, bonusval in bonus.items():
                self._add_remove_bonus(
                    name=name, bonustype=bonustype, bonus=bonusval, add=False)
        else:
            for bonustype in self._appliedbonuses[name]:
                self._add_remove_bonus(
                    name=name, bonustype=bonustype, bonus=0, add=False)
        self._appliedbonuses.pop(name, None)
        if self._recalc:
            self._recalculate_base()

    def get_bonus(self, type: str) -> float:
        """ Total bonus for the given stat """
        return self._totalbonus[type]

    def type_status_chance(self, type: str) -> float:
        """ Chance that any applied status effect will be of the given type """
        if type == self.guaranteed_proc:
            return 1
        else:
            return self.status_chance * (self.damage.get(type, 0)/self._totaldamage)

    def status_effect_count(self, count: int) -> None:
        """ Sets number of different status effects for damage per stat type multipliers"""
        self._statcount = count
        self._recalculate_base()

    def _add_remove_bonus(self, name: str, bonustype: str, bonus: float,
                          add: bool) -> None:
        bonustype = bonustype.lower()
        def _change_bonus(_bonustype: str) -> None:
            if add:
                self._bonuses[_bonustype] |= {name: bonus}
            else:
                self._bonuses[_bonustype].pop(name, None)
            self._totalbonus[_bonustype] = sum(
                self._bonuses[_bonustype].values())

        match bonustype:
            case bonustype if bonustype in ("base", "faction"):
                _change_bonus(bonustype)
                self._recalc = True

            case bonustype if bonustype in (self.base_damage.keys() & set(physical)):
                _change_bonus(bonustype)
                self._moddamage[bonustype] = self.base_damage[bonustype] * \
                    (1 + self._totalbonus[bonustype])
                self._recalc = True

            case bonustype if bonustype in elements:
                _change_bonus(bonustype)
                self._moddamage[bonustype] = self.base_damage.get(
                    bonustype, 0) + self._totalbasedamage * self._totalbonus[bonustype]
                if not self._moddamage[bonustype]:
                    del self._moddamage[bonustype]
                self._recalc = True

            case "atkspd" | "attack speed" | "fire rate":
                _change_bonus("atkspd")
                self.atk_spd = (self.base_atk_spd
                                * (1 + self._totalbonus["atkspd"]))
                self.charge_time = (self.base_charge_time
                                    * ((1 + self._totalbonus["chargetime"])
                                       if self._totalbonus["chargetime"]
                                       else (1/(1 + self._totalbonus["atkspd"]))))
                self.atk_rate = 1/(self.charge_time + 1/self.atk_spd)
                self._tte = self.magazine_size/self.atk_rate
                self._spm = self._tte + self.reload_time

            case "critchance" | "crit" | "crit chance" | "critical chance":
                _change_bonus("critchance")
                self.crit_chance = (self.base_crit_chance
                                    * (1 + self._totalbonus["critchance"]))
                self._avg_crit = 1 + (self.crit_chance*(self.crit_mult - 1))

            case ("critmult" | "crit mult" | "crit multiplier"
                  | "critical multiplier" | 'crit damage' | 'critical damage'):
                _change_bonus("critmult")
                self.crit_mult = (self.base_crit_mult
                                  * (1 + self._totalbonus["critmult"]))
                self._avg_crit = 1 + (self.crit_chance*(self.crit_mult - 1))

            case "statchance" | "stat chance" | "status chance":
                _change_bonus("statchance")
                self.status_chance = (self.base_status_chance
                                      * (1 + self._totalbonus["statchance"]))

            case "chargerate" | "charge rate" | "chargetime" | "charge time":
                _change_bonus("chargetime")
                self.charge_time = (self.base_charge_time
                                    * ((1 + self._totalbonus["chargetime"])
                                       if self._totalbonus["chargetime"]
                                       else (1/(1 + self._totalbonus["atkspd"]))))
                self._tte = self.magazine_size/self.atk_rate
                self._spm = self._tte + self.reload_time

            case "multishot":
                _change_bonus("multishot")
                self.multishot = (self.base_multishot
                                  * (1 + self._totalbonus["multishot"]))
            case "magazine size":
                _change_bonus('magazinesize')
                self.magazine_size = (self.base_magazine_size
                                  * (1 + self._totalbonus['magazinesize']))
                self._tte = self.magazine_size/self.atk_rate
                self._spm = self._tte + self.reload_time
            
            case 'reload time' | 'reload speed':
                _change_bonus('reloadtime')
                self.reload_time = (self.base_reload_time
                                  / (1 + self._totalbonus['reloadtime']))
                self._spm = self._tte + self.reload_time

    def _recalculate_base(self) -> None:
        if (d := self.dmg_per_status) and (s := self._statcount):
            self._bonuses["base"].update(("_dmgperstatus", d*s))
        else:
            self._bonuses["base"].pop("_dmgperstatus", None)
        self._totalbonus["base"] = sum(
            self._bonuses["base"].values())
        self.damage = {
            t: d * (1 + self._totalbonus["base"]) * (1 + self._totalbonus["faction"]) for t, d in self._moddamage.items()}
        self._totaldamage = sum(self.damage.values())

    def _recalc_all(self) -> None:
        for bonustype in (self.base_damage.keys() & set(physical)):
            self._moddamage[bonustype] = self.base_damage[bonustype] * \
                    (1 + self._totalbonus[bonustype])
        for bonustype in elements:
                self._moddamage[bonustype] = self.base_damage.get(
                    bonustype, 0) + self._totalbasedamage * self._totalbonus[bonustype]
        self.atk_spd = (self.base_atk_spd
                        * (1 + self._totalbonus["atkspd"]))
        self.charge_time = (self.base_charge_time
                            * ((1 + self._totalbonus["chargetime"])
                               if self.base_charge_time
                               else (1/(1 + self._totalbonus["atkspd"]))))
        self.atk_rate = 1/(self.charge_time + 1/self.atk_spd)
        self.crit_chance = (self.base_crit_chance
                            * (1 + self._totalbonus["critchance"]))
        self.crit_mult = (self.base_crit_mult
                          * (1 + self._totalbonus["critmult"]))
        self._avg_crit = 1 + (self.crit_chance*(self.crit_mult - 1))
        self.status_chance = (self.base_status_chance
                              * (1 + self._totalbonus["statchance"]))
        self.multishot = (self.base_multishot
                          * (1 + self._totalbonus["multishot"]))
        self.magazine_size = (self.base_magazine_size
                                * (1 + self._totalbonus['magazinesize']))
        self.reload_time = (self.base_reload_time
                            / (1 + self._totalbonus['reloadtime']))
        self._tte = self.magazine_size/self.atk_rate
        self._spm = self._tte + self.reload_time
        self._recalculate_base()

    @property
    def avg_crit(self):
        """ Damage multiplier that adds in expected average crits """
        return 1 + (self.crit_chance*(self.crit_mult - 1))

    @property
    def base_bonus(self):
        return self._totalbonus["base"]

    @property
    def faction_bonus(self):
        return self._totalbonus["faction"]

    @property
    def mod_base_damage(self) -> float:
        """ Base damage modified by base and faction damage mods """
        return self._totalbasedamage * \
            (1 + self._totalbonus["base"] * (1 + self._totalbonus["faction"]))


physical = ("impact", "puncture", "slash")

primaryelements = (
    "cold",
    "electricity",
    "heat",
    "toxin"
)

secondaryelements = (
    "blast",
    "corrosive",
    "gas",
    "magnetic",
    "radiation",
    "viral"
)

elements = primaryelements + secondaryelements

damagetypes = physical + elements
