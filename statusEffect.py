import functools
from weapon import Weapon, damagetypes


@functools.total_ordering
class StatusEffect:
    """
    Status Effect Metaclass.
    Can be called with a damage type to create a specific Status Effect
    """
    @staticmethod
    def proc(type: str, time: float = 0, duration: float = 6):
        match type:
            case "slash":
                return BleedProc(time, duration)
            case "heat":
                return HeatProc(time, duration)
            case "electricity":
                return ElectricityProc(time, duration)
            case "toxin":
                return ToxinProc(time, duration)
            case "corrosive":
                return CorrosiveProc(time, duration)
            case "gas":
                return GasProc(time, duration)
            case "magnetic":
                return MagneticProc(time, duration)
            case "viral":
                return ViralProc(time, duration)
            case _:
                p = StatusEffect(time, duration)
                p.proctype = type
                return p

    proctype = None
    damagetype = None
    damagemulttype = None
    _wpn: Weapon = None

    def __init__(self, time: float, duration: float = 6) -> None:
        self.starttime = time
        self._duration = duration
        self.endtime = time + duration
        self._dmgpertick = 0
        self._t = time
        self._last_t = time
        self._curentticks = 0
        self._totalticks = 0
        self._ended = False

    def __lt__(self, __o: object) -> bool:
        return self.starttime < __o.starttime

    def __eq__(self, __o: object) -> bool:
        return self.starttime == __o.starttime

    def armor_mult(**_) -> float:
        return 1.0

    def dmg_mult(stacks: int) -> float:
        return 1.0

    def update(self, t: float) -> None:
        self._t = t
        self._totalticks = int((t - self.starttime) // 1)
        self._curentticks = int((t - self._last_t) // 1)
        self._ended = t < self.endtime
        self._last_t = t

    @property
    def damage(self) -> float:
        return 0


class BleedProc(StatusEffect):

    damagetype = 'truedmg'
    proctype = 'slash'
        

    @property
    def damage(self) -> float:
        if 0 < self._curentticks < 7:
            dmg = 0
            for _ in range(self._curentticks):
                dmg += (0.35
                        * (w := self._wpn).mod_base_damage
                        * (1 + w.get_bonus('faction'))
                        * w.avg_crit)
            return dmg
        else:
            return 0


class HeatProc(StatusEffect):

    damagetype = 'heat'
    proctype = 'heat'

    @property
    def damage(self) -> float:
        if 0 < self._curentticks < 7:
            dmg = 0
            for _ in range(self._curentticks):
                dmg += (0.5
                        * (w := self._wpn).mod_base_damage
                        * (1 + w.get_bonus('heat'))
                        * (1 + w.get_bonus('faction'))
                        * w.avg_crit)
            return dmg
        else:
            return 0

    def armor_mult(*, time: float, stacks: int) -> float:
        if stacks:
            m = [0, 0.15, 0.3, 0.4, 0.5]
            match time:
                case time if time > 2:
                    return 0.5
                case time if time >= 0:
                    return 1 - m[int(4*time//2)]
                case time if time > -6:
                    return 1 - m[int(-2*time//3)]
                case _:
                    return 1
        else:
            return 1


class ElectricityProc(StatusEffect):

    damagetype = "electricity"
    proctype = "electricity"


    @property
    def damage(self) -> float:
        if 0 <= self._curentticks < 6:
            dmg = 0
            for _ in range(self._curentticks):
                dmg += (0.5
                        * (w := self._wpn).mod_base_damage
                        * (1 + w.get_bonus("electricity"))
                        * (1 + w.get_bonus("faction"))
                        * w.avg_crit)
            return dmg
        else:
            return 0


class ToxinProc(StatusEffect):

    damagetype = "toxin"
    proctype = "toxin"

    @property
    def damage(self) -> float:
        if 0 < self._curentticks < 7:
            dmg = 0
            for _ in range(self._curentticks):
                dmg += (0.5
                        * (w := self._wpn).mod_base_damage
                        * (1 + w.get_bonus("toxin"))
                        * (1 + w.get_bonus("faction"))
                        * w.avg_crit)
            return dmg
        else:
            return 0


class CorrosiveProc(StatusEffect):

    proctype = "corrosive"

    def armor_mult(*, stacks: int, **_) -> float:
        return 1 - (0.2 + 0.06*stacks) if stacks else 1


class GasProc(StatusEffect):

    damagetype = "gas"
    proctype = "gas"

    @property
    def damage(self) -> float:
        if 0 <= self._curentticks < 6:
            dmg = 0
            for _ in range(self._curentticks):
                dmg += (0.5
                        * (w := self._wpn).mod_base_damage
                        * (1 + w.get_bonus("faction"))
                        * w.avg_crit)
            return dmg
        else:
            return 0


class MagneticProc(StatusEffect):

    damagemulttype = "shield"
    proctype = "magnetic"

    def dmg_mult(stacks: int) -> float:
        return 2 + 0.25*(stacks - 1) if stacks else 1


class ViralProc(StatusEffect):

    damagemulttype = "health"
    proctype = "viral"

    def dmg_mult(stacks: int) -> float:
        return 2 + 0.25*(stacks - 1) if stacks else 1

@functools.total_ordering
class AppliedStatusEffects:

    _wpn: Weapon = None
    __threshold = 0.68

    @property
    def threshold(cls) -> float:
        return cls.__threshold

    @threshold.setter
    def threshold(cls, threshold: float) -> None:
        cls.__threshold = threshold

    def __lt__(self, __o: object) -> bool:
        return damagetypes.index(self.proctype) < damagetypes.index(__o.proctype)

    def __eq__(self, __o: object) -> bool:
        return damagetypes.index(self.proctype) == damagetypes.index(__o.proctype)
    
    def __init__(self, *, proc: StatusEffect = None, proctype: str = None) -> None:

        if proc and proctype and proc.proctype != proctype:
            raise Exception("Conflicting proc types given.")
        elif proc:
            self._proclist: list[StatusEffect] = [proc]
        elif not proc and proctype:
            proc = StatusEffect.proc(
                proctype)
            self._proclist: list[StatusEffect] = []
        else:
            raise Exception("Missing proc type.")
        self._assignproctype(proc)
        self._procpercent = 0.0
        self.armormult = 1.0
        self.damagemult = 1.0
        self._count = 0
        self._procchance = 0.0

    def _assignproctype(self, proc: StatusEffect) -> None:
        """
        Sets the proc type for this instance, 
            and sets all variables dependent on type
        """
        self._procclass = type(proc)
        self.proctype = proc.proctype
        if self._wpn and proc._wpn and self._wpn != proc._wpn:
            raise Exception("Different weapon already assigned.")
        elif self._wpn and not proc._wpn:
            StatusEffect._wpn = self._wpn
        elif not self._wpn and proc._wpn:
            self._wpn = proc._wpn
        if self._wpn:
            self._procchance = self._wpn.type_status_chance(
                self.proctype)
            if self.proctype == 'slash' and self._wpn.slashonimpact:
                self._procchance += (self._wpn.slashonimpact 
                                    * self._wpn.type_status_chance('impact'))
        self._initialtime = proc.starttime
        self._expiredtime = proc.endtime
        self.damagetype = proc.damagemulttype
        self.totaldamage = proc.damage

    def add_proc(self, proc: StatusEffect = None, time: float = None) -> None:
        """
        Adds a new proc
        """
        if not self._procclass and not proc:
            raise Exception("No proc type assigned.")
        elif self._procclass and proc and self._procclass != type(proc):
            raise Exception("Different proc type already assigned.")
        elif not self._procclass and proc:
            self._assignproctype(proc)
        elif not proc and time is not None:
            proc = self._procclass(time)
        else:
            raise Exception("Missing proc")
        self._expiredtime = max(self._expiredtime, proc.endtime)
        self._proclist.append(proc)

    def count(self) -> int:
        """
        Total procs for this type
        """
        return len(self._proclist)

    def sort(self) -> None:
        """
        Sorts procs by start time
        """
        self._proclist.sort()

    def update(self, t: float, wpn: Weapon = None) -> None:
        """
        Calculates effects from current procs, 
            and adds new ones if needed
        """
        if wpn and not self._wpn:
            self._wpn = wpn
        if not self._procchance:
            self._procchance = self._wpn.type_status_chance(
                self.proctype)
            if self.proctype == 'slash' and self._wpn.slashonimpact:
                self._procchance += (self._wpn.slashonimpact 
                                    * self._wpn.type_status_chance('impact'))
        self._count = self.count()
        self._procpercent += (self._procchance * wpn.multishot)
        proccount = int(self._procpercent // self.__threshold)
        self._procpercent %= self.__threshold
        if proccount:
            for _ in range(proccount):
                self.add_proc(time=t)
        dt = (t - self._initialtime
              if t < self._expiredtime
              else self._expiredtime - t)
        self.armormult = self._procclass.armor_mult(
            stacks=self._count, time=dt)
        self.damagemult = self._procclass.dmg_mult(self._count)
        self._total_damage(t)

    def _total_damage(self, t: float) -> None:
        """
        Total damage of every proc of this type
        """
        self.totaldamage = 0
        for proc in self._proclist:
            proc.update(t)
            self.totaldamage += proc.damage
            if t > proc.endtime:
                self._proclist.remove(proc)
        if not self._proclist:
            self._initialtime = t
