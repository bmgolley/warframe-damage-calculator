import copy
from enemy import Enemy
from weapon import Weapon
import damagecalc as dc

ap1 = Weapon.game_weapon("Tenet Arca Plasmor")
ap1.set_progenitor_bonus("magnetic", 0.6)
ap1.add_bonuses({
                        "Primed Point Blank": {'base': 1.65},
                        "Galvanized Hell": {'multishot': 1},
                        "Galvanized Savvy": {'status chance': 0.727},
                        "Seeking Fury": {'reload speed': 0.15},
                        "Primed Charged Shell": {'electricity': 1.35},
                        "Vicious Spread": {'base': 0.9},
                        "Primed Ravage": {'critical damage': 1},
                        "Cleanse Corpus": {'faction': 0.3}
                        })

ap2 = Weapon.game_weapon("Tenet Arca Plasmor")
ap2.set_progenitor_bonus('toxin', 0.6)
ap2.add_bonuses({
                        "Primed Point Blank": {'base': 1.65},
                        "Galvanized Hell": {'multishot': 1},
                        "Galvanized Savvy": {'status chance': 0.727},
                        "Seeking Fury": {'reload speed': 0.15},
                        "Primed Charged Shell": {'magnetic': 1.35},
                        "Vicious Spread": {'base': 0.9},
                        "Primed Ravage": {'critical damage': 1},
                        "Chilling Reload": {'magnetic': 0.6, 'reload speed': 0.4}
                        })

er = Enemy.game_enemy("002-Er", 100)
er.base_armor = 50
er.base_shield = 2500
er.base_health = 1000
er.starting_stats(100)
er.reset()
dru = Enemy(100,1,1000,"Flesh",2500,"Shield",50,"Alloy Armor")
er.kill(ap1).to_excel('ap1-er.xlsx')
er.kill(ap2).to_excel('ap2-er.xlsx')
dru.kill(ap1).to_excel('ap1-dru.xlsx')
dru.kill(ap2).to_excel('ap2-dru.xlsx')